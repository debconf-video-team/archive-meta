#!/usr/bin/env python3
import argparse

from utils.files import files_with_prefix, find_used


def main():
    parser = argparse.ArgumentParser(
        description="Find files that aren't contained in a manifest")
    parser.add_argument('prefix', nargs='?', default='',
                        help='Restrict to files with prefix')
    args = parser.parse_args()

    used = set(find_used())

    for video in files_with_prefix(args.prefix):
        if video not in used:
            print(video)


if __name__ == '__main__':
    main()
