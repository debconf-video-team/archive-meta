#!/usr/bin/env python3

import re

import requests
from bs4 import BeautifulSoup

from utils.objects import Event
from utils.yaml import yaml_dump

DESC_RE = re.compile(r'^(?P<title>.*?)(?: \((?P<speakers>[^()]+)\))?$')


def scrape():
    r = requests.get('https://video.debian.net/2006/qa-meeting-badajoz/')

    soup = BeautifulSoup(r.content, 'html.parser')
    for li in soup.find_all('li'):
        speaker = li.get_text().split(':')[0]
        title = li.i.get_text()
        event = Event(
            title=title,
            speakers=[speaker],
            alt_formats={},
        )
        for link in li.find_all('a'):
            href = link['href']
            if href.startswith('mpeg/720x576'):
                event.video = href
                continue
            if href.startswith('xvid'):
                format_ = 'xvid'
            elif href.startswith('ogg_theora/384x288'):
                format_ = 'ogg-low'
            elif href.startswith('ogg_theora/720x576'):
                format_ = 'ogg-high'
            elif href.startswith('mpeg/384x288'):
                format_ = 'mpeg-low'
            event.alt_formats[format_] = href
        yield event


def main():
    events = list(scrape())

    with open('scraped.yml', 'w') as f:
        yaml_dump(events, f)


if __name__ == '__main__':
    main()
