#!/usr/bin/env python3

from difflib import get_close_matches
import datetime
import re

import requests
from bs4 import BeautifulSoup
from html2text import html2text

from utils.objects import Event
from utils.yaml import yaml_dump

FILES = [
    "00-abertura.webm",
    "01-curti-o-debian-quero-contribuir-e-agora.webm",
    "02-assinatura-de-chaves-introducao.webm",
    "03-um-guia-de-newbie-para-newbie-sobre-o-debian.webm",
    "04-o-sistema-operacional-universal-e-a-ciencia.webm",
    "05-como-se-tornar-um-membro-oficial-do-debian(dd-ou-dm).webm",
    "06-releases-oficiais-de-debian-para-cloud.webm",
    "07-reproducible-builds-a-tale-of-three-developers.webm",
    "08-como-obter-ajuda-de-forma-eficiente-sobre-debian.webm",
    "09-boot-seguro-no-debian-quando-e-como.webm",
    "10-debian-e-inteligencia-artificial.webm",
    "11-5-motivos-para-voce-celebrar-o-debianday.webm",
    "12-debian-women-mulheres-no-debian.webm",
    "13-50-years-of-x-and-why-it-is-important.webm",
    "14-palestras-relampago.webm",
    "15-encerramento.webm",
]


def scrape():
    r = requests.get('https://minidebconf.curitiba.br/schedule/')
    soup = BeautifulSoup(r.content, 'html.parser')

    for h2 in soup.find_all('h2'):
        if not h2.get_text().startswith(('13', '14')):
            continue
        date = datetime.date(2018, 4, int(h2.get_text().split(None, 1)[0]))
        table = h2.findNext('table')
        rows = table.find_all('tr')
        for row in rows[1:]:
            cells = row.find_all('td')
            times = cells[0].get_text()
            times = [datetime.datetime.strptime(time, '%H:%M').time()
                    for time in times.partition(' - ')[::2]]
            start = datetime.datetime.combine(date, times[0])
            end = datetime.datetime.combine(date, times[1])
            for room, cell in zip(
                    ('Palestras - Miniauditório', 'Oficinas'),
                    cells[1:]):

                event = Event(
                    title=cell.get_text().strip(),
                    room=room, language='por',
                    start=start, end=end)

                title = cell.find('a', attrs={'class': 'title'})
                if title:
                    event.title = title.get_text()
                    event.description = html2text(
                        title['data-content']).strip()

                speakers = cell.find('a', attrs={'class': 'speaker'})
                if speakers:
                    event.speakers = [
                        speaker.strip()
                        for speaker in speakers.get_text().split(',')]

                yield event


def find_video(meta):
    "Can we find a video for meta? If so, insert it"
    title = meta.title
    slug = title.lower().encode('ascii', 'replace').decode('ascii')
    slug = re.sub('[^a-z0-9()-]', ' ', slug)

    guesses = get_close_matches(slug, FILES)
    if guesses:
        meta.video = guesses.pop(0)
    if guesses:
        meta.guesses = guesses


def main():
    events = []
    for event in scrape():
        find_video(event)
        events.append(event)

    with open('scraped.yml', 'w') as f:
        yaml_dump(events, f)


if __name__ == '__main__':
    main()
